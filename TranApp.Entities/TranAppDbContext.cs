﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TranApp.Entities
{
    public partial class TranAppDbContext : DbContext
    {

        public TranAppDbContext(DbContextOptions<TranAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbBarang> TbBarang { get; set; }
        public virtual DbSet<TbCustomer> TbCustomer { get; set; }
        public virtual DbSet<TbTransaction> TbTransaction { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbBarang>(entity =>
            {
                entity.HasKey(e => e.BarangId)
                    .HasName("PK_BarangId");

                entity.Property(e => e.BarangId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PK_CustomerId");

                entity.Property(e => e.CustomerId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbTransaction>(entity =>
            {
                entity.HasKey(e => e.TransactionId)
                    .HasName("PK_TransactionId");

                entity.Property(e => e.TransactionId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.Barang)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.BarangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BarangId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
