INSERT INTO "TbBarang"("BarangName", "BarangHarga") VALUES ('Nintendo',5000000);
INSERT INTO "TbBarang"("BarangName", "BarangHarga") VALUES ('Xbox',3500000);

INSERT INTO "TbCustomer"("CustomerName","CustomerEmail") VALUES ('Albert','albert@gmail.com');
INSERT INTO "TbCustomer"("CustomerName","CustomerEmail") VALUES ('Owen','owen@gmail.com');

INSERT INTO "TbTransaction"("TransactionDate","TransactionType","IsLunas","TotalTransaction","CustomerId","BarangId")
							VALUES('2020-03-24','Online','1',3500000,'1','6');
INSERT INTO "TbTransaction"("TransactionDate","TransactionType","IsLunas","TotalTransaction","CustomerId","BarangId")
							VALUES('2020-03-25','Offline','1',6000000,'2','5');