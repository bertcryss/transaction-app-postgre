﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbBarang
    {
        public TbBarang()
        {
            TbTransaction = new HashSet<TbTransaction>();
        }

        [Key]
        public int BarangId { get; set; }
        [Required]
        [StringLength(255)]
        public string BarangName { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal BarangHarga { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [InverseProperty("Barang")]
        public virtual ICollection<TbTransaction> TbTransaction { get; set; }
    }
}
