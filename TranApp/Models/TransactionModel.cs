﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class TransactionModel
    {
        public int TransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public BitArray IsLunas { get; set; }
        public decimal? TotalTransaction { get; set; }
        public int CustomerId { get; set; }
        public int BarangId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }

    }
}
