﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class TransactionViewModel
    {
        public int TransactionId { get; set; }
        public string CustomerName { get; set; }
        public string BarangName { get; set; }
        public DateTime? TransactionDate { get; set; }
    }
}
