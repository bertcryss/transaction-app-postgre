﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TranApp.Entities;
using TranApp.Models;

namespace TranApp.Services
{
    public class TransactionService
    {
        private readonly TranAppDbContext _tranDbContext;

        public TransactionService(TranAppDbContext dbContext)
        {
            this._tranDbContext = dbContext;
        }

        public async Task<List<TransactionViewModel>> GetAllTransaction()
        {
            var query = from t in _tranDbContext.TbTransaction
                        join b in _tranDbContext.TbBarang
                        on t.BarangId equals b.BarangId
                        join c in _tranDbContext.TbCustomer
                        on t.CustomerId equals c.CustomerId
                        select new TransactionViewModel
                        {
                            TransactionId = t.TransactionId,
                            CustomerName = c.CustomerName,
                            BarangName = b.BarangName,
                            TransactionDate = t.TransactionDate
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();

            return data;
        }

        public async Task<bool> InsertTransaction(TransactionModel transactionModel)
        {
            this._tranDbContext.Add(new TbTransaction
            {
                TransactionId = transactionModel.TransactionId,
                CustomerId = transactionModel.CustomerId,
                BarangId = transactionModel.BarangId,
                TransactionDate = transactionModel.TransactionDate,
                TransactionType = transactionModel.TransactionType,
                TotalTransaction = transactionModel.TotalTransaction,
                IsLunas = new BitArray(1),
                CreatedAt = transactionModel.CreatedAt,
                CreatedBy = transactionModel.CreatedBy,
                UpdatedAt = transactionModel.UpdatedAt,
                UpdatedBy = transactionModel.UpdatedBy
            });

            await this._tranDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteTransaction(int transactionId)
        {
            var transactionModel = await this._tranDbContext
                .TbTransaction
                .Where(Q => Q.TransactionId == transactionId)
                .FirstOrDefaultAsync();

            if (transactionModel == null)
            {
                return false;
            }

            this._tranDbContext.Remove(transactionModel);
            await this._tranDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<TransactionModel> GetSpecificTransaction(int? transactionId)
        {
            var transactions = await this._tranDbContext
                .TbTransaction
                .Where(Q => Q.TransactionId == transactionId)
                .Select(Q => new TransactionModel
                {
                    TransactionId = Q.TransactionId
                })
                .FirstOrDefaultAsync();

            return transactions;
        }

        public async Task<bool> UpdateTransaction(TransactionModel transactionModel)
        {
            var transactions = await this._tranDbContext
                .TbTransaction
                .Where(Q => Q.TransactionId == transactionModel.TransactionId)
                .FirstOrDefaultAsync();

            if (transactions == null)
            {
                return false;
            }

            transactions.CustomerId = transactionModel.CustomerId;
            transactions.BarangId = transactionModel.BarangId;
            transactions.TransactionType = transactionModel.TransactionType;
            transactions.TotalTransaction = transactionModel.TotalTransaction;
            transactions.TransactionDate = transactionModel.TransactionDate;

            await this._tranDbContext.SaveChangesAsync();
            return true;
        }
    }
}
