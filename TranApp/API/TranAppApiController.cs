﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.API
{
    [Route("api/tran-app")]
    [ApiController]
    public class TranAppApiController : ControllerBase
    {
        private readonly TransactionService _transServiceMan;

        public TranAppApiController(TransactionService transactionService)
        {
            this._transServiceMan = transactionService;
        }

        [HttpGet("all-transaction", Name = "getAllTransaction")]
        public async Task<ActionResult<List<TransactionViewModel>>> GetAllTransactionAsync()
        {
            var data = await this._transServiceMan.GetAllTransaction();
            if (data == null)
            {
                return null;
            }
            return Ok(data);
        }

        [HttpPost("insert-transaction", Name = "insertTransaction")]
        public async Task<ActionResult<ResponseModel>> InsertTransactionAsync([FromBody]TransactionModel value)
        {
            var isSuccess = await this._transServiceMan.InsertTransaction(value);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to insert"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert transaction"
            });
        }

        [HttpDelete("delete-transaction", Name = "deleteTransaction")]
        public async Task<ActionResult<ResponseModel>> DeleteTransactionAsync([FromBody] TransactionModel transactionModel)
        {
            var isSuccess = await this._transServiceMan.DeleteTransaction(transactionModel.TransactionId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to delete"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success to delete transaction {transactionModel.TransactionId}"
            });
        }

        [HttpGet("get-specific-transaction", Name = "getSpecificTransaction")]
        public async Task<ActionResult<TransactionModel>> GetSpecificTransactionAsync(int? transactionId)
        {
            if (transactionId.HasValue == false)
            {
                return BadRequest(null);
            }

            var transaction = await this._transServiceMan.GetSpecificTransaction(transactionId.Value);

            if (transaction == null)
            {
                return BadRequest(null);
            }
            return Ok(transaction);
        }

        [HttpPut("update-transaction", Name = "updateTransaction")]
        public async Task<ActionResult<ResponseModel>> UpdateTransactionAsync([FromBody]TransactionModel transactionModel)
        {
            var isSuccess = await this._transServiceMan.UpdateTransaction(transactionModel);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Transaction ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Update transaction {transactionModel.TransactionId} successfully!"
            });
        }
    }
}